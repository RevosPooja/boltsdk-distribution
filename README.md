# README #
Bolt iOS SDK (BoltSDK) 

BoltSDK is a fast and efficient library written in swift, that wraps locating EV chargers on map, navigating to charger station location, and booking a charger to charge your EV.

## Manual Installation and Setup

Install below mandartory pod files

pod 'CFSDK', '~> 2.1'

pod 'lottie-ios'

pod 'SwiftyJSON', '~> 5.0'

pod 'CryptoSwift', '~> 1.4'

pod 'SwiftDate', '~> 6.3'

pod 'RealmSwift', '~>10'

pod 'Alamofire', '~> 5.6'


### Set up your Project
Drag and drop the BoltSDK.xcframework file onto your Xcode project navigator.

Select the checkbox for ```Copy items if needed```. Click ```Finish```

Then click the + button on ```Embedded Binaries``` under the ```General tab``` of your iOS app project file.

Select ```BoltSDK.xcframework```.

Click ```Finish```.

use ```import BoltSDK``` 

To know more about functions and description please jump to definition or click show quick help from Xcode.

**Download 'BoltEarth-Sample app.zip' for reference**

